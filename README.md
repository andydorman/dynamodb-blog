# NOSQL blog schema/pattern for DynamoDB

This DynamoDB pattern schema aims to provide a blogging service which is:
  * Multi tenanted - it can power a number of different blogs, which I guess could be restricted using an IAM user policy perhaps?
    Restricting access to any items with a secondary key starting with [blog_name|]
  * Allows configuration of categories and tags (per blog), tags can be in categories (or not)
  * Allows blog posts to accept comments which can optionally be required to be manually approved before becoming visible on the front end
  * Posts are both versioned and can be "archived" or "published". Publish dates can be set in the future if you wish.
  * Posts are written in markdown, can be supplied with "brief" and extended properties
  * Post titles are slugified to allow deep linking to posts
  * The blog can be filtered by tag or category or both, this can indicated in the url
  * Comment counts per post are stored, I guess this would be populated on comment approval by way of a lambda triggered from a dynamoDB stream
  * Tag counts are stored, once again this could be populated on comment approval by way of a lambda triggered from a dynamoDB stream.
    This would enable the use of a tag cloud on the front end

## Access patterns

<table>
<tr>
<th colspan="3">End user(?)</th>
</tr?
<tr>
<td>getAllPosts</td><td colspan="2">GSI1 typeTarget = "stlm\|post"<\td>
</tr>
<tr>
<td>getAllArchivedPosts</td><td colspan="2">GSI2 status = "archived", filter begins_with "post|"</td>
</tr>
<tr>
<td>getAllPendingPosts</td><td colspan="2">GSI2 typeTarget = "pending", filter begins_with "post|"</td>
</tr>
<tr>
<td>getAllPublishedPosts</td><td>GSI2 status = "published", filter <= "stlm\|post\|Date.now()"</td><td>aws dynamodb query \<br/>
        --table-name blog \ <br/> --index-name "statusId-queryValue-index" \ <br/> --projection-expression "title, content, tags, category, version" \ i<br/>--key-condition-expression "statusId = :v1 AND queryValue <= :v2" \ <br/> --expression-attribute-values file://expression-attributes/stlm-published-posts.json \ <br/>--return-consumed-capacity TOTAL \ <br/> --profile default</td>
        </tr>
<tr>
<td>getAllPublishedPostsByTag</td><td> GSI2 status = "published", filter <= "stlm|tag|react|Date.now()" </td><td> aws dynamodb query \ <br/> --table-name blog \ <br/> --index-name "statusId-queryValue-index" \ <br/>--projection-expression "title, content, tags, category, version" \ <br/> --key-condition-expression "statusId = :v1 AND queryValue <= :v2" \ <br/> --expression-attribute-values file://expression-attributes/stlm-published-posts-by-tag.json \ <br/> --return-consumed-capacity TOTAL \ <br/> --profile default</td>
</tr>
</table>

    * getAllPublishedPostsByCategory - GSI2 status = "published", filter <= "stlm|category|technology|Date.now()"
      * aws dynamodb query \
        --table-name blog \
        --index-name "statusId-queryValue-index" \
        --projection-expression "title, content, tags, category, version" \
        --key-condition-expression "statusId = :v1 AND queryValue <= :v2" \
        --expression-attribute-values file://expression-attributes/stlm-published-posts-by-category.json \
        --return-consumed-capacity TOTAL \
        --profile default
    * getPublishedPostBySlug - GSI2 status = "published", filter <= "2019-03-20", statusDate begins_with "published|stlm|post|first-post"
    * getAllCommentsByPost - id = aa10d5d0-4460-11e9-b475-0800200c9a66, typeTarget begins_with "stlm|post|aa10d5d0-4460-11e9-b475-0800200c9a66|comment"
      * aws dynamodb query \
        --table-name blog \
        --projection-expression "queryValue, content, createdAt" \
        --key-condition-expression "id = :v1 AND begins_with(typeTarget,:v2)" \
        --expression-attribute-values file://expression-attributes/stlm-comments-by-post.json \
        --return-consumed-capacity TOTAL \
        --profile default
    OR
    * getAllApprovedCommentsByPost - GSI2 status = "approved", filter begins_with "stlm|post|uuid1"
      * aws dynamodb query \
        --table-name blog \
        --index-name "statusId-queryValue-index" \
        --projection-expression "queryValue, content, createdAt" \
        --key-condition-expression "statusId = :v1 AND begins_with(queryValue, :v2)" \
        --expression-attribute-values file://expression-attributes/stlm-approved-comments-by-post.json \
        --return-consumed-capacity TOTAL \
        --profile default
    * getCommentCountByPost - id = uuid1, typeTarget  "stlm|post|comment_count"
    OR
    * getApprovedCommentCountByPost - GSI2 status = "approved", filter begins_with "stlm|post|uuid1|comment_count"

  backend
    * getAllPostVersions - id = uuid1, typeTarget begins_with "stlm|v"

    * getUntaggedPosts - GSI1 typeTarget = stlm|post|no_tags
    * getUncategorisedPosts - GSI1 typeTarget = stlm|post|no_categories

    * getAllPostVersions - id = uuid1, begins_with "stlm|post|v"
    * getPostVersion - id = uuid, typeTarget begins_with "stlm|post|v1"

    * getAllTags - GSI1 typeTarget = "stlm|tag"
      * const [tag, category] = tag.split('|'); to build an interface that allowed the selection of tags by category (or not)

    * getAllCategories - GSI1 typeTarget = "stlm|category"
    * getAllTagsByCategory - GSI1 typeTarget = "stlm|tag|category|technology"


Assumptions:
  * typeTarget = [blog]|post is always a copy of the most recent version. It may or may not be published/archived
  * dates would be unix timestamps

| id (PK)  |  typeTarget SK(GS1 (PK))            |             filter GSI1 (SK), GSI2 (SK)                     |               status GSI2 (PK)     | Version...Title...Content...Tags...Category...Author |
|----------|-------------------------------------|-------------------------------------------------------------|------------------------------------|--------------------------------------------------------------------|
| uuid1    | stlm\|post                           |            stlm\|post\|2019-03-09                             |       published      |      2
| uuid1    | stlm\|post\|published                 |           stlm\|post\|first-post\|2019-03-09                   |       published      |      2
| uuid1    | stlm\|post\|published\|tag\|1           |            stlm\|tag\|react\|2019-03-09                        |       published      |      2
| uuid1    | stlm\|post\|published\|tag\|2           |            stlm\|tag\|redux\|2019-03-09                        |       published      |      2
| uuid1    | stlm\|post\|published\|1\|category\|1    |            stlm\|category\|technology\|2019-03-09              |       published      |      2
| uuid1    | stlm\|post\|published\|1\|category\|1    |            stlm\|category\|technology\|tag\|react\|2019-03-09    |       published      |      2
| uuid1    | stlm\|post\|published\|1\|category\|1    |            stlm\|category\|technology\|tag\|redux\|2019-03-09    |       published      |      2
| uuid1    | stlm\|v1\|post                        |            stlm|post\|my-first-post\|2019-03-01               |                      |
| uuid1    | stlm\|v1\|post\|category\|1             |            stlm\|category\|technology                         |                      |
| uuid1    | stlm\|v2\|post\|first-post             |            stlm\|2019-03-04                                  |                      |
| uuid1    | stlm\|v2\|post\|tag\|uuid4              |            stlm\|tag\|react                                   |                      |
| uuid1    | stlm\|v2\|post\|tag\|uuid5              |            stlm\|tag\|redux                                   |                      |
| uuid1    | stlm\|v2\|post\|category\|uuid7         |            stlm\|category\|technology                         |                      |

### Comments - could use the version number so these *could* be hidden if post changed version?

| id (PK)  |  typeTarget SK(GS1 (PK))            |             filter GSI1 (SK), GSI2 (SK)                     |               status GSI2 (PK)     | Version...Title...Content...Tags...Category...Author |
|----------|-------------------------------------|-------------------------------------------------------------|------------------------------------|--------------------------------------------------------------------|
| uuid1     | stlm\|post\|comment_2019-03-10                 |   stlm\|post\|uuid1\|comment_2019-03-10        |              approved (could default all to approved?)  |
| uuid1     | stlm\|post\|comment_2019-03-10\|comment_2019-03-12 | stlm\|post\|uuid1\|comment_2019-03-10\|comment_2019-03-12 |  approved  |
| uuid1     | stlm\|post\|comment_2019-03-11                    | stlm\|post\|uuid1\|comment_2019-03-11                  |    approved  |
| uuid1     | stlm\|post\|comment_2019-03-12                    | stlm\|post\|uuid1\|comment_2019-03-12                  |    approved  |
| uuid1     | stlm\|post\|comment_count                         | stlm\|post\|uuid\|comment_count|4

### A second post

| id (PK)  |  typeTarget SK(GS1 (PK))            |             filter GSI1 (SK), GSI2 (SK)                     |               status GSI2 (PK)     | Version...Title...Content...Tags...Category...Author |
|----------|-------------------------------------|-------------------------------------------------------------|------------------------------------|--------------------------------------------------------------------|
| uuid2     | stlm\|post                                       stlm\|2019-03-13
| uuid2     | stlm\|v1\|post                                    stlm\|my-second-post
| uuid2     | stlm\|v1\|post\|category\|1                         stlm\|category\|gaming

### Blogs(tenants)/Tags/Categories

| id (PK)  |  typeTarget SK(GS1 (PK))            |             filter GSI1 (SK), GSI2 (SK)                     |               status GSI2 (PK)     | Version...Title...Content...Tags...Category...Author |
|----------|-------------------------------------|-------------------------------------------------------------|------------------------------------|--------------------------------------------------------------------|
| uuid3  |   blog                                   |         stlmi                                        |
| uuid3  |   blog\|url                              |          http://scalingtheleadmountain.co.uk         |
| uuid4  |   stlm\|tag                              |          react\|technology                           |
| uuid4  |   stlm\|tag\|category\|technology        |            react                                     |
| uuid5  |   stlm\|tag                              |          redux\|technology                           |
| uuid5  |   stlm\|tag\|category\|technology        |            redux                         |
| uuid6  |   stlm\|tag                              |          netrunner\|gaming       |
| uuid6  |   stlm\|tag\|category\|gaming            |            netrunner       |
| uuid7  |   stlm\|category                         |          technology          |
| uuid8  |   stlm\|category                         |          gaming            |


### A second blog(tenant)

| id (PK)  |  typeTarget SK(GS1 (PK))            |             filter GSI1 (SK), GSI2 (SK)                     |               status GSI2 (PK)     | Version...Title...Content...Tags...Category...Author |
|----------|-------------------------------------|-------------------------------------------------------------|------------------------------------|--------------------------------------------------------------------|
| uuid9  |   blog                           |                 aomega
| uuid9  |   blog\|url                       |                 http://aomegasolutions.com
| uuid10 |   aomega\|tag                      |                react
| uuid10 |   aomega\|tag\|category\|js        |                  react
| uuid11  |  aomega\|js                       |                redux
| uuid11  |  aomega\|tag\|category\|js        |                  redux
| uuid12  |  aomega\|tag                      |                dynamodb
| uuid12  |  aomega\|tag\|category\|aws       |                  dynamodb
| uuid13  |  aomega\|category                 |                js
| uuid14  |  aomega\|category                 |                aws
| uuid15  |  aomega\|post                     |                aomega\|2019-03-13
| uuid15  |  aomega\|v1\|post                 |                 aomega\|my-second-post
| uuid15  |  aomega\|v1\|post\|no_categories  |
| uuid15  |  aomega\|v1\|post\|no_tags      |
