import uuid from 'uuid';
import * as dynamoDbLib from './libs/dynamodb-lib';
import { success, failure } from './libs/response-lib';

export async function main(event, context, callback) {
  // Request body is passed in as a JSON encoded string in 'event.body'
  const data = JSON.parse(event.body);
  const {
    title = '',
    brief,
    content = '',
    tags = [],
    category,
    status = 'published',
  } = data;
  const TableName = 'stlm';

  const now = Date.now();
  const id = `post_${now}`;
  let putItems = [];
  const postItem = {
    id,
    related_id: `v_0#${status}`,
    userId: event.requestContext.identity.cognitoIdentityId,
    title,
    content: {
      extended: content,
    },
    updatedAt: now,
    createdAt: now,
  };

  if (brief) {
    postItem.content.brief = brief;
  }

  if (!tags.length) {
    putItems.push({ ...postItem, related_id: 'tags_0' });
  } else {
    postItem.tags = tags;
  }

  if (!category) {
    putItems.push({ ...postItem, related_id: 'category_0' });
  } else {
    postItem.category = category;
  }

  putItems.push(postItem);

  const { id: post_id, related_id: post_related_id, ...postAttrs } = postItem;
  putItems = [
    ...putItems,
    ...tags.map(tag => ({
      id: post_id,
      related_id: `tag_${tag}#v_0#${status}`,
      ...postAttrs,
    })),
  ];

  if (category) {
    putItems = [
      ...putItems,
      {
        id: post_id,
        related_id: `catgeory_${category}#v_0#${status}`,
        ...postAttrs,
      },
      ...tags.map(tag => ({
        id: post_id,
        related_id: `category_${category}#tag_${tag}#v_0#${status}`,
        ...postAttrs,
      })),
    ];
  }

  try {
    const params = {
      RequestItems: {
        [TableName]: putItems.map(Item => ({ PutRequest: { Item } })),
      },
      ReturnConsumedCapacity: 'TOTAL',
    };

    await dynamoDbLib.call('batchWrite', params);
    return success(params);
  } catch (e) {
    console.log(putItems);
    return failure({ status: false });
  }
}
